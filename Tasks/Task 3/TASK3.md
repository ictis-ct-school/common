# Ваш первый анализ данных
В рамках решения второго задания вы попробуете себя в роли Data Science специалиста.

## Задача
- выбрать одну криптовалюту из списка;
- загрузить данные и создать блокнот в Google Collab;
- произвести целевой анализ данных;
- выбрать целевой признак;
- решить задачу кроссвалидации;
- выбрать модель для обучения;
- построить ROC-кривые;
- похвастаться своим опытом финтехе.

### Условия выполнения
- наличие одного и более целевых признаков;
- обучение не менее, чем на трёх моделях.


### Пример входных и выходных данных
Выбор в рамках датасета.

### Оценка
Каждое решение будет оформлено как значимый пункт в резюме.
Преподаватели тоже участвуют в решении задачи.
