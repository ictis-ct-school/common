# Дедлайны жмут, пора работать!

Данный этап обучения подразумевает полный процесс реализации проекта, включая распределение ролей, определение дедлайнов и непосредственную реализацию его. 

У вас уже есть репозитории, есть команды и, главное, есть время. Пора начать работу над проектом.

## Менеджмент

Одним из самых сложных элементов реализации является организация рабочего процесса. Необходимо понять, что из себя представляет проект, какие у вас есть ресурсы для реализации и как их правильно распределить для эффективной деятельности.

Сначала подумайте над тем, **что из себя представляет проект** и на какие составляющие его можно **поделить**(всегда следуйте концепции: "разделяй и властвуй").
В вашем случае, реализация может включать в себя:

```mermaid
graph TD;
    A[Ваш супер крутой проект] --> B[Интерфейс пользователя];
    A[Ваш супер крутой проект] --> C[Серверная часть];
    B[Интерфейс] --> D[Фронтенд];
    B[Интерфейс] --> E[Бэкенд];
    D[Фронтенд] --> F[Дизайн];
    D[Фронтенд] --> G[Верстка];
    E[Бэкенд] --> H[Обработка событий];
    E[Бэкенд] --> I[Взаимодействие с сервером];
    C[Серверная часть] --> J[Обработка запросов];
    C[Серверная часть] --> K[Часть с машинным обучением];

```

После определения структуры нужно понять, за сколько реально реализовать проект. С таймменджентом у всех всё плохо, так что придётся постараться, чтобы не проворонить дедлайн.
Чтобы спасти души грешные от кары заказчиков, умные люди придумали различные инструменты для проджект менеджеров, которые остаются актуальными и по сей день.

Реализуя данный проект мы будем работать в рамках одной из них, самой актуальной для нас.
[Scrum....](https://www.atlassian.com/ru/agile/scrum)
