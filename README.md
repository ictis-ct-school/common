# Учебная база

Данный репозиторий создан для помощи в работе с обучающимися

## Стек работы в рамках курса
[![pipeline status](https://img.shields.io/badge/OpenCV-090909?style=for-the-badge&logo=OpenCV)](https://gitlab.com/ictis-ct-school/common)
[![pipeline status](https://img.shields.io/badge/Keras-090909?style=for-the-badge&logo=Keras)](https://gitlab.com/ictis-ct-school/common)
[![pipeline status](https://img.shields.io/badge/scikitlearn-090909?style=for-the-badge&logo=scikitlearn)](https://gitlab.com/ictis-ct-school/common)
[![pipeline status](https://img.shields.io/badge/PyTorch-090909?style=for-the-badge&logo=PyTorch)](https://gitlab.com/ictis-ct-school/common)

## Организация рабочих мест
### Команда "CodeHunters"
- [Даниил Лавров](@lavrovdaniil622)(капитан)
- [Максим Сёмкин](@28022008maks)
- [Илья Боровой](@GellionFenett)
- [Роман Скубриев](@roma4769)
- [Максим Нагреба](@nagrebamax)
- [Тимофей Капсомун](@timofycapsomun)
### Команда "PS"
- [Даниил Григорьев](@Megadeth2006)(капитан)
- [Дмитрий Одинцов](@DmodvGL)
- [Лилия Здрановская](@lilia02123)
- [Анастасия Переверзева](@Anastasia2306)
- [Андрей Галуза](@prostoandr)
### Команда "Nackers_team"
- [Андрей Дордопуло](@Andrey366448)(капитан)
- [Ярослав Дорошенко](@ckil299)
- [Матвей Долбин](@D.M.C)
- [Вячеслав Стротинский](@Vstrotinlav)
- [Евгений Романов](@R.E.D)
### Команда "2d2ms"
- [Даниил Клименченко](@WinDincs)(капитан)
- [Алексей Жигалин](@Nifilx)


## Документация
- [Помощь в оформлении .md файлов](https://docs.gitlab.com/ee/user/markdown.html)
